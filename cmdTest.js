const num1 = parseFloat(process.argv[2]);
const num2 = parseFloat(process.argv[3]);

if (isNaN(num1) || isNaN(num2)) {
  console.log("Please provide valid numbers.");
} else {
  const sum = num1 + num2;
  console.log(process.argv);
  console.log("The sum of the two numbers is:", sum);
}