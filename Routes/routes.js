
const express = require('express');
const router = express.Router();
const upload = require('../config/mutlter-configs')
const Audio = require('../model/Audio');



router.get('/',(req, res)=>{

    res.render('index',{message: req.query.message || ''});
})

router.get('/voice-input', (req, res)=>{
    res.render('voiceInput');
})

router.post('/audio/upload', upload.single('audio'), (req, res)=>{
    console.log(req.files);
    console.log(req.file);
    console.log(req.body);
    const filename= req.file.filename, path= req.file.path;
    const audio = new Audio({
        filename: filename,
        path: path
    });

    audio.save()
    .then((audio)=>{
        console.log('Audio saved:', audio);
        // res.status(200).json({message:'Upload successfully'});
        res.redirect('/?message=Upload successfully')
    })
    .catch((error)=>{
        console.log(error);
        res.status(500).json({error: 'Error uploading audio'});
    })
});

module.exports = router;