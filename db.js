const mongoose = require('mongoose');
const mongoURI = 'mongodb://localhost:27017/Audio';
// using async/await
async function connectToMongoDB() {
    try {
      await mongoose.connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true });
      console.log('Connected to MongoDB');
    } catch (error) {
      console.error('Error connecting to MongoDB:', error);
    }
  }
// using then
mongoose.connect(mongoURI, {useNewUrlParser:true, useUnifiedTopology:true})
.then(()=>{
    console.log('connected');
})
.catch((error)=>{
    console.log(error);
});

// connectToMongoDB()